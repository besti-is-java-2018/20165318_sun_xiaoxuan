import java.util.*;
class Student1 implements Comparable {
   int english=0;
   String name;
   Student1(int english,String name) {
      this.name=name;
      this.english=english;
   }
   public int compareTo(Object b) {
      Student1 st=(Student1)b;
      return (this.english-st.english);
   }
}
public class Example15_8 {
  public static void main(String args[]) {
     TreeSet<Student1> mytree=new TreeSet<Student1>();
     Student1 st1,st2,st3,st4;
     st1=new Student1(90,"赵一");
     st2=new Student1(66,"钱二");
     st3=new Student1(86,"孙三");
     st4=new Student1(76,"李四");
     mytree.add(st1);
     mytree.add(st2);
     mytree.add(st3);
     mytree.add(st4);
     Iterator<Student1> te=mytree.iterator();
     while(te.hasNext()) {
        Student1 stu=te.next();
        System.out.println(""+stu.name+" "+stu.english);
     }
  }
}
