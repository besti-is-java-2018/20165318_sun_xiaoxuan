import java.sql.*;
public class city{
    public static void main(String args[]) {
        Connection con;
        Statement sql;
        ResultSet rs;
        con = GetDBConnection.connectDB("world","root","sunxiaoxuan001");
        if(con == null ) return;
        String sqlStr =
                "select * from city where Population>1016531";
        try {
            sql=con.createStatement();
            rs = sql.executeQuery(sqlStr);
            while(rs.next()) {
                int ID=rs.getInt(1);
                String Name=rs.getString(2);
                String CountryCode=rs.getString(3);
                String District=rs.getString(4);
                long Population=rs.getLong(5);
                System.out.printf("%d\t",ID);
                System.out.printf("%s\t",Name);
                System.out.printf("%s\t",CountryCode);
                System.out.printf("%s\t",District);
                System.out.printf("%d\n",Population);
            }
            con.close();
        }
        catch(SQLException e) {
            System.out.println(e);
        }
    }
}
