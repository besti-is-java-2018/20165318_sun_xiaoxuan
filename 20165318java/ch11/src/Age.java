import java.sql.*;
public class Age {
    public static void main(String args[]) {
        Connection con;
        Statement sql;
        ResultSet rs;
        float agemax = 0f;
        float agemin = 100f;
        String namemax = null;
        String namemin = null;
        con = GetDBConnection.connectDB("world","root","sunxiaoxuan001");
        if(con == null ) return;
        String sqlStr = "select * from country";
        try {
            sql = con.createStatement();
            rs = sql.executeQuery(sqlStr);
            while (rs.next()) {
                String Name = rs.getString(2);
                float p = rs.getFloat(8);
                if (agemax <= p) {
                    agemax = p;
                    namemax = Name;
                }
                if (p < agemin&&p!=0) {
                    agemin = p;
                    namemin = Name;
                }
            }
            System.out.printf("%s\t",namemax);
            System.out.printf("%f\n",agemax);
            System.out.printf("%s\t",namemin);
            System.out.printf("%f\n",agemin);
            con.close();
        }
        catch(SQLException e) {
            System.out.println(e);
        }
    }
}