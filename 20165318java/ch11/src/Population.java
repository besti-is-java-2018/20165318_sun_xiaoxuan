import java.sql.*;
public class Population {
    public static void main(String args[]) {
        Connection con;
        Statement sql;
        ResultSet rs;
        long sum = 0;
        con = GetDBConnection.connectDB("world","root","sunxiaoxuan001");
        if(con == null ) return;
        String sqlStr = "select * from country where Region = 'Middle East'";
        try {
            sql=con.createStatement();
            rs = sql.executeQuery(sqlStr);
            while(rs.next()) {
                long p = rs.getLong(7);
                sum = sum +p;
            }
            System.out.println(sum);
            con.close();
        }
        catch(SQLException e) {
            System.out.println(e);
        }
    }
}