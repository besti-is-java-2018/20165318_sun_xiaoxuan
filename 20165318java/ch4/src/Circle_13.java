public class Circle_13 {
	double radius;
	double area;
	void setRadius(double r) {
		radius = r;
	}
	double getArea() {
		area = 3.14*radius*radius;
		return area;
	}
}