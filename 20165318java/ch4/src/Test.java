class CPU {
	int speed;
	int getSpeed() {
		return speed;
	}
	void setSpeed(int m) {
		speed = m;
	}
}
class HardDisk {
	int amount;
	int getAmount() {
		return amount;
	}
	void setAmount(int m) {
		amount = m;
	}
}
class PC {
	CPU cpu = new CPU();
	HardDisk HD = new HardDisk();
	void setCPU(CPU c) {
		cpu = c;
	}
	void setHardDisk(HardDisk h) {
		HD = h;
	}
	void show() {
		System.out.println("CPU的速度:"+cpu.getSpeed()+"\n"+"硬盘的容量:"+HD.getAmount());
	}
}
public class Test{
	public static void main(String args[]) {
		CPU cpu = new CPU();
		cpu.setSpeed(2200);
		HardDisk disk = new HardDisk();
		disk.setAmount(200);
		PC pc = new PC();
		pc.setCPU(cpu);
		pc.setHardDisk(disk);
		pc.show();
	}
}