class Tom {
	private int x = 120;
	protected int y = 20;
	int z = 11;
	private void f() {
		x = 200;
		System.out.println(x);
	}
	void g() {
		x = 200;
		System.out.println(x);
	}
}
public class E2_6 {
	public static void main(String args[]) {
		Tom tom = new Tom();
		tom.x = 22;
		tom.y = 33;
		tom.z = 55;
		tom.f();
		tom.g();
	}
}