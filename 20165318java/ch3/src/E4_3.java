public class E4_3 {
	public static void main(String args[]) {
		double sum=0,n=1;
		int m=20,i=0;
		do {
			i = i+1;
			n = n*(1.0/i);
			sum = sum + n;
		}while(i<=20);
		System.out.println("使用do-while循环计算的sum="+sum);
		for(int j=1;j<=20;j++) {
			n = n*(1.0/j);
			sum = sum + n;
		}
		System.out.println("使用for循环计算的sum="+sum);
	}
}