import junit.framework.TestCase;
import org.junit.Test;

public class NifixToSuffixTest extends TestCase {
    NifixToSuffix e1 = new NifixToSuffix();
    @Test
    public void testNifixToSuffix() throws Exception {
        e1.conversion("1 + 2 * ( 2 + 3 )");
        assertEquals("1 2 2 3 + * + ", e1.getMessage());
    }
}