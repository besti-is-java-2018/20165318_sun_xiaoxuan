import junit.framework.TestCase;
import org.junit.Test;

public class RationalNumberTest extends TestCase {
    RationalNumber r1 = new RationalNumber(1,3);
    RationalNumber r2 = new RationalNumber(1,2);
    @Test
    public void testGetNumerator() throws Exception {
        assertEquals(1,r1.getNumerator());
    }
    @Test
    public void testGetDenominator() throws Exception {
        assertEquals(3,r1.getDenominator());
    }
    @Test
    public void testReciprocal() throws Exception {
        assertEquals("3",(r1.reciprocal()).toString());
    }
    @Test
    public void testAdd() throws Exception {

        assertEquals("5/6",(r1.add(r2)).toString());
    }
    @Test
    public void testSubtract() throws Exception {
        assertEquals("-1/6",(r1.subtract(r2)).toString());
    }
    @Test
    public void testMultiply() throws Exception {
        assertEquals("1/6",(r1.multiply(r2)).toString());
    }
    @Test
    public void testDivide() throws Exception {
        assertEquals("2/3",(r1.divide(r2)).toString());
    }
}