import junit.framework.TestCase;
import org.junit.Test;

public class CalculatorTest extends TestCase {
    Calculator c = new Calculator();
    @Test
    public void testIsOperator() throws Exception {//测试运算符
        assertEquals(true,c.isOperator("+"));
        assertEquals(true,c.isOperator("-"));
        assertEquals(true,c.isOperator("*"));
        assertEquals(true,c.isOperator("÷"));
    }
    @Test
    public void testEvaluate() throws Exception {//测试后缀式计算
        assertEquals("3",c.evaluate("1 2 +"));
        assertEquals("0",c.evaluate("1 1 -"));
        assertEquals("2",c.evaluate("1 2 *"));
        assertEquals("3",c.evaluate("3 1 ÷"));
    }
    @Test
    public void testTranIntoRationalNum() throws Exception {//测试真分数化简
        assertEquals("1/3",(c.tranIntoRationalNum("3/9")).toString());
    }
}