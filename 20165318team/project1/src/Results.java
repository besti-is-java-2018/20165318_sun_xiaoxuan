import java.io.*;
import java.text.NumberFormat;
import java.util.StringTokenizer;

public class Results {
    public static void main(String[] args) throws IOException{
    //public OutputResultToFile() throws IOException {
            Calculator jdg = new Calculator();
        NifixToSuffix nts = new NifixToSuffix();
        NumberFormat fmt = NumberFormat.getPercentInstance();
        FileInputStream fis = new FileInputStream("Exercises.txt");
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader in = new BufferedReader(isr);
        StringTokenizer tokenizer1 = null, tokenizer2 = null;
        String token1, token2, token3, token4;
        String s1 = null;
        String str;
        int q = 0, count = 0;
        PrintStream ps = new PrintStream("ExercisesResult.txt");
        while ((str=in.readLine()) != null) {
            tokenizer1 = new StringTokenizer(str, ":");
            token1 = tokenizer1.nextToken();
            token2 = tokenizer1.nextToken();
            tokenizer2 = new StringTokenizer(token2, "=");
            token3 = tokenizer2.nextToken();
            token4 = tokenizer2.nextToken();
            nts.conversion(token3);
            if (token4.equals(jdg.evaluate(nts.getMessage()))) {
                s1 = "正确！";
                q++;
            } else {
                s1 = "错误，正确答案为：" + jdg.evaluate(nts.getMessage());
            }

            String s2 = str + "\n" + s1 + "\n\n";
            ps.append(s2);
            count++;
        }

        double accuracy = (double) q / count;
        String s3 = "完成" + count + "道题目，答对" + q + "道题，正确率为" + fmt.format(accuracy);
        ps.append(s3);


}
}