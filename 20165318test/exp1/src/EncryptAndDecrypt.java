public class EncryptAndDecrypt {
    String encrypt(String sourceString, int password) throws EadException{   //加密算法
        char[] c = sourceString.toCharArray();
        int m = c.length;
        for (int i = 0; i < m; i++) {
            if(c[i]>122||c[i]<65||(c[i]>90&&c[i]<97)){
                throw new EadException(sourceString);
            }
            int mima = (c[i] - 'a' + password) % 26 + 'a';          //加密
            c[i] = (char) mima;
        }
        return new String(c);                                       //返回密文
    }

    String decrypt(String sourceString, int password) throws EadException{   //解密算法
        char[] c = sourceString.toCharArray();
        int m = c.length;
        for (int i = 0; i < m; i++) {
            if(c[i]>122||c[i]<65||(c[i]>90&&c[i]<97)){
                throw new EadException(sourceString);
            }
            int n = c[i] - 'a' - password;
            if (n <= 0) {
                n = n + 26;
            }
            int mima = n % 26 + 'a';          //解密
            c[i] = (char) mima;
        }
        return new String(c);                          //返回明文
    }
}

