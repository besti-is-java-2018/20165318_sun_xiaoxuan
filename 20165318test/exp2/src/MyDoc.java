/**
 * Created by sxx on  2018/4/14.
 */
//Server Classes
abstract class Data{
    public abstract void DisplayValue();
}
class Integer extends Data {
    int value;
    Integer(){
        value=100;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}
class Byte extends Data{
    byte value;
    Byte(){
        value=(byte)18;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}
//Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class ByteFactory extends Factory {
    public Data CreateDataObject(){
        return new Byte();
    }
}
//Client Classes
class Document {
    Data pd;
    Document(Factory pf) {
        pd=pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test Classes
public class MyDoc {
    static Document d;
    public static void main(String[] args) {
        d = new Document(new ByteFactory());
        d.DisplayData();
    }
}