import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by sxx on 2018/4/14.
 */
public class StringBufferDemoTest extends TestCase {
    StringBuffer string1 = new StringBuffer("Students");
    StringBuffer string2 = new StringBuffer("Students of class 1653");
    StringBuffer string3 = new StringBuffer("Students of class 1653 and class 1652");
    @Test
    public void testCharAt() throws Exception{
        assertEquals('t',string1.charAt(1));
        assertEquals(' ',string2.charAt(8));
        assertEquals('1',string3.charAt(18));
    }
    @Test
    public void testCapacity() throws Exception{
        assertEquals(24,string1.capacity());
        assertEquals(38,string2.capacity());
        assertEquals(53,string3.capacity());
    }
    @Test
    public void testindexOf() throws Exception{
        assertEquals(1, string1.indexOf("tud"));
        assertEquals(8, string2.indexOf(" of"));
        assertEquals(18, string3.indexOf("1653"));
    }
    @Test
    public void testlength() throws Exception{
        assertEquals(8, string1.length());
        assertEquals(22, string2.length());
        assertEquals(37, string3.length());
    }
}