/**
 * Created by SXX on 2018/4/27
 */


public class Caesar {

    public static void main(String[] args) throws EadException {
        String s = args[0];
        int key = Integer.parseInt(args[1]);
        String es = "";

        for (int i = 0; i < (s.length()); i++) {
            char c = s.charAt(i);
            //自定义异常类
            try {
                boolean exist = ((c > 64) && (c < 91)) || ((c > 96) && (c < 123) || c == 32 || c == 33);
                if (exist == false) {
                    throw new EadException(s);
                }
            } catch (EadException e) {
                System.out.println(e.warnMess());
            }
            //小写字母
            if (c >= 'a' && c <= 'z') {
                //移动key%26位
                c += key % 26;
                //向左超界
                if (c < 'a') {
                    c += 26;
                }
                //向右超界
                if (c > 'z') {
                    c -= 26;
                }
            }

            //大写字母
            else if (c >= 'A' && c <= 'Z') {
                c += key % 26;
                if (c < 'A') {
                    c += 26;
                }
                if (c > 'Z') {
                    c -= 26;
                }
            }
            es += c;
        }
        System.out.println(es);

    }
}





