import javax.crypto.Cipher;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.security.Key;

public class SEnc {
    public String Cipher(String s) throws Exception {
        FileInputStream f = new FileInputStream("key1.dat");
        ObjectInputStream b = new ObjectInputStream(f);
        Key k = (Key) b.readObject();
        Cipher cp = Cipher.getInstance("DESede");
        cp.init(Cipher.ENCRYPT_MODE, k);
        byte ptext[] = s.getBytes("UTF8");
        System.out.println("密文为：");
        for (int i = 0; i < ptext.length; i++) {
            System.out.print(ptext[i] + ",");
        }
        System.out.println("");
        byte ctext[] = cp.doFinal(ptext);
        String cipher = "";
        for (int i = 0; i < ctext.length; i++) {
            cipher = cipher + ctext[i];
        }
        FileOutputStream f2 = new FileOutputStream("SEnc.dat");
        f2.write(ctext);
        return cipher;
    }
}