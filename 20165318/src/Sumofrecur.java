import java.util.Arrays;
public class Sumofrecur {
	public static void main(String [] args) {
		long sum = 0;
		long i;
		if (Integer.parseInt(args[0])<=0) {
			System.out.println("error!");
		}
		else{
			for(i=1;i<=(Integer.parseInt(args[0]));i++) {
				sum = sum + fact(i);
			}
			System.out.println("sum = "+sum);
		}
	}
	public static long fact(long n) {
		if(n==0) {
			return 1;
		}
		else {
			return n * fact(n-1);
		}
	}
}


