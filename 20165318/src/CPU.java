public class CPU {
    int speed;
    CPU (){

    }
    CPU (int m) {
        this.speed = m;
    }
    public void setSpeed(int m) {
        this.speed = m;
    }
    public int getSpeed(){
        return speed;
    }

    @Override
    public String toString() {
        return "CPU("+"speed="+speed+")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        CPU cpu = (CPU)obj;
        return getSpeed() == cpu.getSpeed();
    }
}
