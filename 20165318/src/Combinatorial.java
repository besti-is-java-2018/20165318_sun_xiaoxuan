import java.util.*;
public class Combinatorial {
    public static void main(String[] args) {
        int temp [] = new int [args.length];
        int c;
        for(int i=0;i<args.length;i++){
            temp[i] = Integer.parseInt(args[i]);
        }
        c = fact(temp[0],temp[1]);
        if(c==0){
            System.out.println("error!");
        }
        else{
            System.out.println(c);
        }
    }
    public static int fact(int n,int m) {
        if(m==0 || n==0 || n==m) {
            return 1;
        }
        else if(m==1){
            return n;
        }
        else if(m>n){
            return 0;
        }
        else {
            return fact(n-1,m-1)+fact(n-1,m);
        }
    }
}
