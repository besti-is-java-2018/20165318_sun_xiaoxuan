import java.util.Objects;

public class HardDisk {
    HardDisk(){

    }
    HardDisk (int m) {
        this.amount = m;
    }
    public int getAmount() {
        return amount;
    }

    public void setAmount(int m) {
        this.amount = m;
    }

    int amount;

    @Override
    public String toString() {
        return "HardDisk(" +
                "amount=" + amount +
                ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        HardDisk hardDisk = (HardDisk) obj;
        return getAmount() == hardDisk.getAmount();
    }

}