# 20165318 2017-2018-2 《Java程序设计》第二周学习总结
## 教材学习内容总结
本周学习了第二章和第三章的内容，掌握了Java中基本数据类型、数组、运算符、表达式和语句等方面的知识。

总结第一周的学习经验，在本周的学习过程中，我仔细阅读了教材，发现了很多以往在学习C语言中没有注意到的小细节。在与C语言对比学习的过程中，不仅巩固了C语言基础，而且对Java的便利有了新的认识，仿佛打开了一扇大门，在编写代码时不再觉得枯燥无味，反而会主动思考，并用不同的方法去解决同一个问题。

### Java与C语言的不同之处
1、Java中没有无符号的byte，short，int和long，这一点和C语言不同。因此，unsigned int m;是错误的s变量声明。

2、Java中的char型数据一定是无符号的，而且不允许使用unsigned来修饰所声明的char型变量。

3、Java不允许在声明数组中的方括号内指定数组元素的个数。例如int a[12]；将导致语法错误。

4、Java允许使用int型变量的值指定数组的元素的个数。例如：
```
int size = 30;
double number[] = new double[size];
```

### 基本数据类型与数组
- 逻辑类型：`boolean`，赋值是只能使用`true`和`false`，不能使用`0`和`1`；

- 对`long``float`型变量赋值时，后缀不能省略；

- 对`char`型变量赋值时，既可以用`'A'`（即用单引号括起）的形式，也可以用字符在Unicode表中的排序位置赋值，但char型变量没有负数。
```
char x = 'a';
char x = 97;
```
以上两种变量声明等价。

- 在Java中，可以用字符在Unicode表中的排序位置的十六进制转义（需要用u做前缀）来表示该字符，其一般格式为`'\u****'`，其中只能有四位数字。

- 基本数据类型变量按精度从低到高排列：
```
byte short char int long float double
```

- 数据转换有两种形式：
    1、当把级别低的变量的值赋给级别高的变量时，系统自动完成数据类型的转换；
    2、当把级别高的变量的值赋给级别低的变量时，必须使用类型转换运算。

- Scanner是JDK1.5新增的一个类，可以使用该类创建一个对象：`
Scanner reader = new Scanner (System.in);`然后reader对象调用下列方法，读取用户在命令行输入的各种基本数据类型：
```
nextBoolean(),nextByte(),nextShort(),nextShort(),nextInt(),nextLong(),nextFloat(),nextDouble()
```
上述方法执行时都会堵塞，程序等待用户在命令行输入数据回车确认。在从键盘输入数据时，经常让reader对象先调用`hasNextXXX()`方法等待用户在键盘输入数据，然后再调用`nextXXX()`方法获取用户输入的数据。

- 声明数组时可以一次声明多个数组，声明数组和创建数组可以一起完成`float boy[] = new float[4];`。

- 构成二维数组的一维数组不必有相同的长度。

- length的使用：对于一维数组，“数组名.length”的值就是数组中元素的个数；对于二维数组，“数组名.length”的值是它含有的一维数组的个数。

- 对于char型数组a，System.out.println(a)输出数组a的全部元素的值。`System.out.println(""+a)`输出char型数组的引用。

### 运算符、表达式和语句

- 逻辑运算符`&&`和`||`也称作短路逻辑运算符。

- `instanceof`运算符：左面的操作元是一个对象，右面的是一个类，当左面的对象是右面的类或子类创建的对象是，运算结果为`true`，否则为`false`。

- `switch`语句中表达式的值可以是`byte`、`short`、`int`、`char`型，但不可以是`long`型数据。

- JDK1.5对for语句的功能给予了扩充，以便更好地遍历数组：`for(声明循环变量：数组的名字)`，这种表示方法可以理解为“对于循环变量依次取数组的每一个元素的值”。

- `for`语句中，声明的循环变量不能使用已经声明过的变量。

## 教材学习中的问题和解决过程
- 问题1：类型转换运算(P21)

在讲把级别高的变量的值赋给级别低的变量，必须使用类型转换运算时，有一个例子：
```
long y = (long)56.98F
```
我当时很疑惑为什么float型变量赋给long型变量时要使用类型转换运算。
- 解决过程：在第二次看教材时发现，我混淆了float和long的精度高低。基本数据类型变量按精度从低到高排列：
```
byte short char int long float double
```

- 问题2：类型转换过程中精度损失（P22）

教材中有例子：
```
byte a = (byte)128;
byte b = (byte)(-129);
```
得到的a和b的值分别时-128和127，我不理解这个代码的运算过程。
- 解决过程：在[类型转换运算](https://www.zybang.com/question/ff08650895c5d44819c4734f2b3050d9.html)中得到了解决，并自己尝试计算
```
byte a = (byte)255;
```
得到的结果的确是a=-1。

- 问题3：数组的引用（P32）
课后习题中有一题代码是：
```
public class E_5 {
	public static void main (String args[]) {
		int [] a={10,20,30,40},b[]={{1,2},{4,5,6,7}};
		b[0] = a;
		b[0][1] = b[1][3];
		System.out.println(b[0][3]);
		System.out.println(a[1]);
	}
}
```
输出结果是40和7，
我认为应该输出的是40和20。

- 解决过程： 在有一次学习数组引用的相关知识后，我明白了`b[0] = a;`是将数组a和b[0]的引用变为了同一个，在执行`b[0][1]=b[1][3];`时，a数组的值也随之变化，因此a[1]的值变成了7。

## 代码调试过程中的问题和解决过程

- 问题1：我使用的是Windows中的git bash，但是在git 中会出现中文乱码的问题，我的git中默认的编码是UTF-8，我在vim中编写代码后用编译运行就会出现中文乱码。我参考了[windows下git bash中文乱码解决办法 ](http://blog.csdn.net/u013068377/article/details/52168434),但在这与我的git情况不符。我在自己尝试后，发现使用GBK编码运行不会出现中文乱码，但是在vim编辑器中会出现中文乱码。
这个问题我现在还没有找到解决方法。

![输入图片说明](https://gitee.com/uploads/images/2018/0310/222918_47cc0f4d_1676247.png "Example2_1.PNG")


- 问题2：
我在调试例子3.9（P47）时出现错误

![](https://gitee.com/uploads/images/2018/0310/222906_9e0314a5_1676247.png "Example3_9.PNG")

- 解决过程：经过检查代码，我发现，我将`System.out.printf`,输入成`System.out.println`，导致`println`与其内容中的`%d`不符。

- 问题3：在调试例子3.7（P45）输出100以内的z所有素数时，结果只能输出2和3代码如下；
```
public class Example3_7 {
	public static void main (String args[]) {
		int sum =0,i,j;
		for(i=1;i<=10;i++) {
			if(i%2==0) {
				continue;
			}
			sum = sum + i;
		}
		System.out.println("sum="+sum);
		for(j=2;j<=100;j++) {
			for(i=2;i<=j/2;i++){
				if(j%i==0) { 
					break;
				}
			}
			if(i>j/2) {
				System.out.println(""+j+"是素数");
			}
		}
	}
}
```
- 解决过程：由于我打代码时都会使用行尾风格，并且即使是`if`语句中只有一行代码也不会省略{}，因此在输入书上代码时，我将`if`语句中的`break;`语句也用{}括起来了，但是输出素数却只有2和3，但我去掉`break;`语句的{}时运行结果正常。

虽然能输出正确结果了，但我还是不太明白问题出在哪，难道`break`语句只跳出了`if`语句却没有跳出`for`循环吗？希望能得到解答。

码云链接：https://gitee.com/BESTI-IS-JAVA-2018/20165318_sun_xiaoxuan

脚本运行截图：

![输入图片说明](https://gitee.com/uploads/images/2018/0310/222930_579c5aed_1676247.png "ch2,3.PNG")


参考资料：

[windows下git bash中文乱码解决办法 ](http://blog.csdn.net/u013068377/article/details/52168434)

[类型转换运算](https://www.zybang.com/question/ff08650895c5d44819c4734f2b3050d9.html)