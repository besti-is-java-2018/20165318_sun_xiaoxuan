# 20165318 2017-2018-2 《Java程序设计》第一周学习总结

## 教材内容学习总结

第一章主要对Java平台进行了简单的介绍，并讲解了如何搭建Java环境。

### Java平台概论

通过课本，测验，查询资料我了解到Java的有关内容如下：

- Java历史

1990年Sun公司成立了有James Gosling领导的开发小组，开始正致力于开发一种可移植的、跨平台的语言，该语言能生成正确运行于各种操作系统及各种CPU芯片上的代码。1995年5月Sun公司推出的Java Development Kit 1.0a2版本，标志着Java的诞生。Java的出现标志着分布式系统的真正到来。

- Java三大平台

Java SE（Java 2 Platform Standard Edition标准版）
Java EE（Java 2 Platform Enterprise Edition企业版）
Java ME（Java 2 Platform Micro Edition微型版）

- Java SE的四个组成部分

JVM：Java虚拟机，包含在JRE中
JRE：Java执行环境，运行Java程序必需（包括Java SE API,JVM,部署技术）
JDK：包括JRK及开发过程中需要的工具，如javac、java等工具程序，开发Java程序必需

如果只是要运行Java程序，只要有JRE程序即可。
只要平台提供了Java运行环境，Java编写的软件就能在其上运行。

### DOS命令

由于我用的是Windows中不能用bash，很多Linux环境下的命令不可使用，因此我在网上搜索了DOS的命令以便使用。

dir: 列出当前目录下的文件以及文件夹

md: 创建目录

rd: 删除目录

cd: 进入指定目录

del: 删除文件

copy： 复制文件

xcopy: 复制目录

tree: 列出目录树

ren: 文件改名

type: 显示文件内容

cls： 清屏

exit: 退出DOS命令行

##学习过程遇到的问题及解决方法

由于我在出现出现问题时没有截图，因此一下只有问题和解决方法，并没有截图

Q：安装好JDK之后，在命令行输入Java正常，但是输入javac是错误，会提示没有此命令。

A：使用path E:\jdk1.9\bin;%path%后在当前DOS命令行窗口暂时可以使用。

Q：javac可以使用后，使用java不能编辑，即使设置classpath环境变量后还是不行。

A：由于我最初安装时没有没有按书上选择目录，所以有的环境变量可能设置的有错误，因此我重装了一次JDK，但是JDK1.9中没有jre文件，我查到可
以.;%java_home%\lib\dt.jar;%java_home%\lib\tools.jar;来设置classpath,问题得到解决，并能成功运行java程序。
![classpath](https://gitee.com/uploads/images/2018/0304/163614_b40156aa_1676247.png "classpath.PNG")

Q：由于班级组织建立的比较晚，我之前在个人中创建了一个项目，并且可以从本地传到码云上。我今天在组织上创建项目，并按同样步骤在git bash上操作
时，发现git push origin master不能上传，如下图

![](https://images2018.cnblogs.com/blog/1296414/201803/1296414-20180304162630190-187493585.jpg)

A：参考[使用git推送代码到开源中国以及IDEA环境下使用git](http://www.cnblogs.com/rebrust/p/5348866.html)，输入命令

```
git pull origin master
git push origin master
```

结果还是不行，就使用强推指令，使本地代码强行覆盖远程仓库文件,强推指令为

```
git push -f origin master
```

问题得以解决。

码云链接： [BESTI.IS.JAVA2018 / 20165318_sun_xiaoxuan](https://gitee.com/BESTI-IS-JAVA-2018/20165318_sun_xiaoxuan/tree/master)

脚本运行截图：
![](https://gitee.com/uploads/images/2018/0304/163456_baa3801a_1676247.png "ch1.PNG")