# 20165318 2017-2018-2 《Java程序设计》第三周学习总结

## 学习总结
我感觉从这一章开始，新的知识点扑面而来，很多定义都是之前没有接触过的，看书的时候难免有些晦涩。但由于老师的要求，我在寒假期间提前预习过教材配套PPT，虽然很多知识点都记不太清了，但比刚开始学习陌生的东西强很多。而且看书的时候配合着例题一起练习，的确对知识点的掌握有很大的帮助，我会保持“看+练”的方式继续学习以后课程的学习。

## 教材学习总结

#### 1. 面向对象编程的特征
 
    - 封装性
    - 继承性
    - 多态性

#### 2.类

Java程序的组成：一个Java应用程序由若干个类所构成
    
- 类：包括类声明和类体两部分。
```
class 类名 {
    类体的内容
}
```
- 类体：包括变量的声明和方法的定义两部分

- 变量：分为成员变量（域变量）和局部变量

PS: 成员变量与局部变量的区别：

(1)成员变量在声明变量部分声明，在方法体之外；局部变量为方法体中声明的变量和方法的参数。

(2)成员变量在整个类中都有效，其有效性与它在类体中书写的先后位置无关；局部变量只在方法中有效，而且从声明它的位置之后开始有效。

(3)如果局部变量的名字与成员变量的名字相同，那么成员变量被隐藏，如果想使用被隐藏的局部变量，必须使用关键字`this`。

(4)成员变量由默认值，局部变量没有默认值。

- 方法：由方法头和方法体构成
```
方法头 {
    方法体的内容
}
```

- 注意：

    对成员变量的操作只能放在方法中。

    在使用局部变量之前，必须保证局部变量有具体的值，否则编译无法通过。

- UML图：属于结构图，常被用于描述一个系统的静态结构。一个UML中通常包含类的UML图、接口的UML图、泛化关系的UML图、关联关系的UML图、依赖关系的UML图和实现关系的UML图。
    

#### 3.构造方法与对象的创建

- 对象：用类声明的变量

- 构造方法：类中的一种特殊方法，当程序用类创建对象是需使用它的构造方法。

    (1)类中的构造方法的名字必须与它所在的类的名字完全相同。

    (2)构造方法没有类型。

    (3)允许在一个类中编写若干个构造方法，但必须保证他们的参数不同。

PS：参数不同：

(1)参数的个数不同。

(2)参数个数相同，但参数列表中对应的某个参数的类型不同。

- 创建对象：包括对象的声明和为对象分配变量两个步骤。
```
Lader lader;        //声明对象
lader = new Lader();//为对象分配变量（使用new和构造方法）
```
与`Lader lader = new Lader();`相同。

如果成员变量在声明时没有指定初值：

    - 整型成员变量，默认初值为0；
    - 浮点型成员变量，默认初值为0.0；
    - boolean型成员变量，默认初值为false；
    - 引用型变量，默认初值为null；

- 实体：分配给对象的变量。

    注意：没有实体的对象称为空对象，空对象不能使用，否则运行时会出现异常。
- Java与C+ +的区别：
    C++中有构造方法和析构方法，程序员要时刻自己检查哪些对象应该使用析构方法释放内存。
    Java语言中，类有构造方法，但没有析构方法，Java运行环境有“垃圾收集”机制，其发现堆中分配的实体不再被栈中任何对象所引用时，就会释放该实体在t堆中占用的内存。

如果希望Java虚拟机立即进性“垃圾收集”操作，可以让`System`类调用`gc()`方法。

- 尽管一个Java源程序中可以有多个类，但仍然提倡在一个Java源文件中只编写一个类。

#### 4.参数传值

-  传值机制：方法中参数变量的值是调用者指定的值的拷贝。

- 两种方式：基本数据类型参数的传值，引用类型参数的传值。 

- 对于基本数据类型的参数，向该参数传递的值的级别不可以高于该参数的级别。

- 可变参数：在声明方法时不给出参数列表中从某项开始直至最后一项参数的名字和个数。

    注意：但这些参数的类型必须相同，并且最后一个参数必须是方法的参数列表中的最后一个参数。
```
public void f(int ... x)
```

- 对于可变参数，Java提供了增强的for语句，可以按如下方式使用for语句遍历参数代表所代表的参数：
```
for(声明循环变量：参数代表) {
    ...
}
```

#### 5.对象的组合

如果一个对象a组合了对象b，那么对象a就可以委托对象b调用其方法，即对象a以组合的方式复用对象b的方法。

- 关联关系：如果A类中的成员变量是用B类声明的对象，那么A和B的关系是关联关系，称A类的对象关联于B类的对象或A类的对象组合了B类的对象。
- 依赖关系：如果A类中某个方法的参数是用B类声明的对象或某个方法返回的数据类型是B类对象，那么A和B的关系是依赖关系，称A依赖于B。

#### 6.实例成员与类成员

###### 实例变量和类变量
- 类变量：在声明变量时，用关键字`static`给予修饰的变量，也称为static变量、静态变量。
- 实例变量：不用`static`修饰的变量。
- 实例变量和类变量的区别

    (1)不同对象的实例变量互不相同
    
    (2)所有对象共享类变量
    
    (3)通过类名直接访问类变量
    
###### 实例方法和类方法

- 实例方法：方法声明时，方法类型前面不加关键字`static`修饰的方法。

- 类方法：方法声明时，方法类型前面加关键字`static`修饰的方法。

- 实例方法与类方法的区别：
    
    (1)实例方法既可以操作实例变量也可以操作类变量；类方法只能操作类变量，不可以操作实例变量。
    
    (2)实例方法只能由对象来调用，不能通过类名调用；类方法既能由对象调用，也能直接通过类名调用。

    (3)当类被加载到内存时，其中的类方法就被分配了相应的入口地址；而实例方法在该类创建对象后，才分配入口地址。
    
- Arrays类：

`public static void sort(double a[])`方法可以把参数a指定的double类型数组按升序排序；

`public static void sort(double a[],int start,int end)`方法可以把参数a指定的double类型数组中索引star~end-1的元素的值按升序排序；

`public static int binarySearch(double a[],double number)`方法判断参数number指定的数值是否在参数a指定的数组中；

#### 7.方法重载

- 方法重载：一个类中可以有多个方法具有相同的名字，但这些方法的参数必须不同。

#### 8.this关键字

- `this`可以出现在实例方法和构造方法中，但不能出现在类方法中。
- 当实例成员变量在实例方法中出现时，默认的格式是：`this.成员变量`,其中`this.`可省略。
- 当static成员变量在实例方法中出现时，默认的d格式是：`类名.成员变量`，其中`类名.`可省略。
- 当实例成员变量的名字和局部变量的名字相同时，成员变量前面的`this.`或`类名.`不可省略。

#### 9.包

- 包语句的格式为:
```
package 包名;
```
- 如果使用包名，运行主类时，主类全名是`包名.主类名`。

#### 10.import语句
- 一个类想要使用的类和它不在一个包里时可以用import语句使用这样的类 ，例如`import java.util.*`。

#### 11.访问权限
- 用关键字`private`修饰的成员变量和方法成为私有变量和私有方法。
- 用关键字`public`修饰的成员变量和方法成为共有变量和共有方法。
- 用关键字`protected`修饰的成员变量和方法成为受保护的变量和受保护的方法。
- 不用关键字`private`、`public`、`protected`修饰的成员变量和方法成为友好变量和友好方法。
- 不能用`protected`和`private`修饰类。
- 访问限制修饰符按访问权限从高到低的排列顺序是`public`、`protected`、`友好`、`private`。

#### 12.对象数组
- 对象数组的定义：
```
Student [] stu;
stu = new Student[10];
```
这些对象都是空对象，在使用之前，需要创建数组所包含的对象，例如：`stu[0] = new Student()；`。

#### 13.JRE扩展与jar文件

需要编写一个清单文件：hello.mf。
```
Manifest-Version: 1.0
Class: moon.star.TestOne moon.star.TestTwo
Created-By: 1.8
```
## 遇到的问题与解决方法

- 问题1：由于上次课上做实验时使用Windows系统下的Java编辑系统不能调试，因此我在虚拟机上又重新安装了一次JDK，但Linux中与Windows中安装步骤不同，我不知道如何安装。
- 解决方法：参考[Ubuntu16.04下Java环境安装与配置 ](http://blog.csdn.net/oh_mourinho/article/details/52691398)

(1)在虚拟机中下载JDK-1.8压缩包并进行解压

(2)使用$ su root进入root权限。可能会提示你密码错误，使用$ sudo passwd重新设置密码即可。
参考网址：su 认证失败

(3)编辑 /etc/profile文件设置环境变量,在文件末尾添加以下代码：
```
export JAVA_HOME=/JDK下载的位置
export JRE_HOME=$JAVA_HOME/jre
export CLASSPATH=.:$CLASSPATH:$JAVA_HOME/lib:$JRE_HOME/lib
export PATH=$PATH:$JAVA_HOME/bin:$JRE_HOME/bin
```
(4) 使用source/etc/profile使刚才的配置信息生效。

(5) 使用java -version 检验是否安装成功。如果发现没有成功可以检查环境变量是否输入错误，也可以重启虚拟机进行尝试。

- 问题2：在调试Example4_5代码时，我发现无法直接使用`javac -d bin src/Example4_5.java`命令使该代码的字节码文件直接放入bin目录。

![](https://images2018.cnblogs.com/blog/1296414/201803/1296414-20180318150956854-550468156.png)


- 解决方法：在看过蓝墨云班课中同学的提问后，我发现可以使用`javac -d bin src/*.java`进性编译，但是由于我的第四章的代码都在同一个目录下，这样编译会很麻烦，于是我使用`javac -d bin src/Example4_5.java src/Rect.java src/Lader.java`,问题得以解决。

![](https://images2018.cnblogs.com/blog/1296414/201803/1296414-20180318151012939-1922631995.png)


- 问题3：在调试Example4_15时，我在bin目录的上级目录使用`java -cp . : bin tom.jiafei.Example4_15`运行程序，出现以下问题：

![](https://images2018.cnblogs.com/blog/1296414/201803/1296414-20180318151112839-1105931216.png)

- 解决方法：我使用`java -cp bin tom.jiafei.Example4_15`命令就可以运行，虽然问题解决了，但我暂时还不清楚到底为什么。

![](https://images2018.cnblogs.com/blog/1296414/201803/1296414-20180318151447734-1456139770.png)

- 问题4：我在虚拟机上装好JDK，git后，将码云上的代码传到了虚拟机上，但发现带有汉字的源文件在虚拟机上不能编译
![](https://images2018.cnblogs.com/blog/1296414/201803/1296414-20180318151618722-326448957.png)

- 解决方法：我在参考[javac编译错误: 编码UTF8/GBK的不可映射字符 ](http://blog.csdn.net/leytton/article/details/52740171)后发现Linux下为UTF-8编码，Windows下为GBK编码，Linux中javac编译gbk编码的java文件时，容易出现“错误: 编码UTF8的不可映射字符”。现在可以用命令`javac -encoding gbk *.java`编译成功。


## 码云链接：https://gitee.com/BESTI-IS-JAVA-2018/20165318_sun_xiaoxuan/tree/master/

## 代码托管：

![](https://images2018.cnblogs.com/blog/1296414/201803/1296414-20180318152313034-1437583706.png)

## 上周考试错题总结
1、 表达式-2 >>> 1的值为 

    A . 2^31 – 1 
    B . 2^15 – 1 
    C . 1
    D . -1 
    答案：A
2、表达式-2 >> 1的值为 

    A . 2^31 – 1 
    B . 2^15 – 1 
    C . 1 
    D . -1 
在学习第三章中运算符时，位移运算有`>>`,`<<`,`>>>`三种，我不理解这`>>`和`>>>`的区别，在查阅[移位运算符](https://baike.baidu.com/item/移位运算符/5622348?fr=aladdin)后得到解答，`>>`是带符号右移，而`>>>`是无符号右移。

3、表达式15&250的值为 

    A . FF 
    B . 0 
    C . 255 
    D . 1 
我认为答案应该是10，但选项中并没有我计算的答案。

4、下列说法正确的是 
 
    A . Java语言有8种基本数据类型。 
    B . 基本数据类型可分为逻辑类型、整数类型、字符类型、浮点类型。 
    C . short和Float都属于基本数据类型。 
    D . int a = 0144中的0144是十六进制表示法。
我选择的是AB，关于C答案，我用程序试了一下，使用Float定义可以编译和运行，但我不知道这个选项是否正确。

5、在命令行输入“java demo 3”，下列程序的运行结果为
```
public class unsigned {
    public static void main(String args[]) {
        int i = Integer.parseInt(args[0]);
        switch(i) {
            case 1:System.out.println("Spring");break;
            case 2:System.out.println("Summer");break;
            case 3:System.out.println("Autumn");break;
            case 4:System.out.println("Winter");break;
        }
    }
} 
A . Spring 
B . Summer 
C . Autumn 
D . Winter 
```
我选择的答案是C，但我不知道这个程序具体是如何运行的。

## 参考资料
[Ubuntu16.04下Java环境安装与配置 ](http://blog.csdn.net/oh_mourinho/article/details/52691398)
[javac编译错误: 编码UTF8/GBK的不可映射字符 ](http://blog.csdn.net/leytton/article/details/52740171)
[移位运算符](https://baike.baidu.com/item/移位运算符/5622348?fr=aladdin)