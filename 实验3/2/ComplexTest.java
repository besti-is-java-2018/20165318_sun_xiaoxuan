import junit.framework.TestCase;
import org.junit.Test;
/**
 * Created by czz on 2018/4/27
 */
public class ComplexTest extends TestCase {
    Complex c1 = new Complex(0.0, 3.0);
    Complex c2 = new Complex(-1.0, -1.0);
    Complex c3 = new Complex(2.0,1.0);
    @Test
    public void testgetRealpart() throws Exception{
        assertEquals(0.0,c1.getRealPart());
        assertEquals(-1.0,c2.getRealPart());
        assertEquals(2.0,c3.getRealPart());
    }
    @Test
    public void testgetImagePart() throws Exception{
        assertEquals(3.0,c1.getImagePart());
        assertEquals(-1.0,c2.getImagePart());
        assertEquals(1.0,c3.getImagePart());
    }
    @Test
    public void testComplexAdd() throws Exception{
        assertEquals("-1.0+2.0i",c1.ComplexAdd(c2).toString());
        assertEquals("2.0+4.0i",c1.ComplexAdd(c3).toString());
        assertEquals("1.0",c2.ComplexAdd(c3).toString());
    }
    @Test
    public void testComplexSub() throws Exception{
        assertEquals("1.0+4.0i",c1.ComplexSub(c2).toString());
        assertEquals("-2.0+2.0i",c1.ComplexSub(c3).toString());
        assertEquals("-3.0-2.0i",c2.ComplexSub(c3).toString());
    }
    @Test
    public void testComplexMulti() throws Exception{
        assertEquals("3.0-3.0i",c1.ComplexMulti(c2).toString());
        assertEquals("-3.0+6.0i",c1.ComplexMulti(c3).toString());
        assertEquals("-1.0-3.0i",c2.ComplexMulti(c3).toString());
    }
    @Test
    public void testComplexDiv() throws Exception{
        assertEquals("-1.5-1.5i",c1.ComplexDiv(c2).toString());
        assertEquals("1.2+0.375i",c1.ComplexDiv(c3).toString());
        assertEquals("-0.6-0.375i",c2.ComplexDiv(c3).toString());
    }
    public void testtoString() throws Exception{
        assertEquals("3.0i",c1.toString());
        assertEquals("-1.0-1.0i",c2.toString());
        assertEquals("2.0+1.0i",c3.toString());
    }
}
